import { LightningElement, api, wire, track } from 'lwc';
/* Wire adapter to fetch record data */
import getContacts from '@salesforce/apex/mylwcController.getContacts';
//import searchContacts from '@salesforce/apex/mylwcController.searchContacts';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import ACCOUNT_OBJECT from '@salesforce/schema/Account';
import NAME_FIELD from '@salesforce/schema/Account.Name';
import ID_FIELD from '@salesforce/schema/contact.Id';
import FIRSTNAME_FIELD from '@salesforce/schema/contact.FirstName';
import LASTNAME_FIELD from '@salesforce/schema/contact.LastName';
import EMAIL_FIELD from '@salesforce/schema/contact.Email';
import {refreshApex} from '@salesforce/apex';
import {updateRecord} from 'lightning/uiRecordApi';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';

const COLS = [
    { label: 'First Name', fieldName: 'FirstName', editable: true },
    { label: 'Last Name', fieldName: 'LastName', editable: true },
    { label: 'Email', fieldName: 'Email', type: 'email', editable: true}
];
const DELAY = 300;

export default class mylwc extends LightningElement {
    @track error; 
    @track columns = COLS; 
    @track draftValues = [];  
    //@track searchKey = '';

    @wire(getContacts, {accId : '$recordId'})
    contact;

    //@wire(searchContacts, {searchKey : '$searchKey', accId : '$recordId'})
    //contact;

    /** Id of record to display. */
    @api recordId;

    /*handleKeyChange(event) {
        window.clearTimeout(this.delayTimeout);
        const searchKey = event.target.value;
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.delayTimeout = setTimeout(() => {
            this.searchKey = searchKey;
        }, DELAY);
    }*/

    handleSave(event){
        const fields = {};
        fields[ID_FIELD.fieldApiName] = event.detail.draftValues[0].Id;
        fields[FIRSTNAME_FIELD.fieldApiName] = event.detail.draftValues[0].FirstName;
        fields[LASTNAME_FIELD.fieldApiName] = event.detail.draftValues[0].LastName;
        fields[EMAIL_FIELD.fieldApiName] = event.detail.draftValues[0].Email;
        const recordInput = {fields};

        updateRecord(recordInput)
        .then(() => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success',
                    message: 'Contact updated',
                    variant: 'success'
                })
            );
            // Clear all draft values
            this.draftValues = [];

            // Display fresh data in the datatable
            return refreshApex(this.contact);
        }).catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error creating record',
                    message: error.body.message,
                    variant: 'error'
                })
            );
        });
    }

    /* Expose schema objects/fields to the template. */
    accountObject = ACCOUNT_OBJECT;
    /* Load Account.Name for custom rendering */
    @wire(getRecord, { recordId: '$recordId', fields: [NAME_FIELD] })
    record;
    /** Get the Account.Name value. */
    get nameValue() {
        return this.record.data ? getFieldValue(this.record.data, NAME_FIELD) : '';
    }
}