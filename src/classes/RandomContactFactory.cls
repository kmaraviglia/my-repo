public class RandomContactFactory {
	
    public static List<Contact> generateRandomContacts(Integer n, String stringa){
        List<Contact> contacts = new List<Contact>();
        for(Integer i=1; i<=n; i++){
            Contact c = new Contact(FirstName='Test'+i);
            contacts.add(c);
        }
        
        return contacts;
    }
}