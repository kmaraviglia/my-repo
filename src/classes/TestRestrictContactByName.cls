@isTest
public class TestRestrictContactByName {
    @isTest static void  RestrictContactTest(){
        Contact c = new Contact(LastName = 'INVALIDNAME');
        
        Test.startTest();
		Database.SaveResult result = Database.insert(c);
        Test.stopTest();
        
        System.assert(!result.isSuccess());
        System.assert(result.getErrors().size() > 0);
        System.assertEquals('The Last Name "'+c.LastName+'" is not allowed for DML', result.getErrors()[0].getMessage());
    }
}