global class LeadProcessor implements Database.Batchable<sObject>{
    global Integer recordsProcessed = 0;

    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator('SELECT Name, Id, LeadSource FROM Lead'
        );
    }
    
    global void execute (Database.BatchableContext bc, List<Lead> records){
        for(Lead l : records){
            l.LeadSource = 'Dreamforce';
            recordsProcessed = recordsProcessed + 1;
        }
        update records;
    }
    
    global void finish(Database.BatchableContext bc){
        System.debug(recordsProcessed + ' records processed. Shazam!');
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, 
            JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob
            WHERE Id = :bc.getJobId()];
    }
}