public class CertificationAttemptTriggerHandler {

	public static void createCertificationHeld(List<Attempt__c> certAttempts, Map<Id, Attempt__c> oldMap) {

		/**
		* If a Contact successfully passes a Certification Attempt, then the code
		* will gather the data necessary to perform a query for related Certification
		* Attempt records.
		*/

		System.Debug('Calling the createCertificationHeld method');

        // The set of candidates and certifications. Used to filter SOQL query when looking at existing cert attempt records
        Set<Id> candidates = new Set<Id>();
        Set<Id> certifications = new Set<Id>();

        /** TODO #1: Declare a variable that is a List of Certification_Attempt__c sObjects named certAttemptsPass
		 *           and initialize it a newly constructed list of Certification_Attempt__c sObjects.
		 */
        List<Attempt__c> certAttemptsPass = new List<Attempt__c>();

		Map<String, Integer> passCounts = new Map<String, Integer>();

        /** TODO #2: Declare a variable of type Map that uses the type String for the key and the
		 *           type Certification_Held_c for the value named certsHeldToAdd and initialize
		 *           it as a newly constructed instance of this map type.
		 */
        Map<String, Held__c> certsHeldToAdd = new Map<String, Held__c>();

        // Extract candidates and certifications from incoming records that have the status 'Complete/Pass'
        for(Attempt__c attempt : certAttempts){
            if (attempt.Status__c== 'Complete/Pass' && (oldMap==null || oldMap.get(attempt.Id).Status__c!='Complete/Pass')) {
                candidates.add(attempt.Student__c);
                certifications.add(attempt.Certification_Id__c);

                /** TODO #3: Call the add method for the set class on the certAttemptsPass variable passing in
				 *           the variable named attempt as the element to add.
				 */
                certAttemptsPass.add(attempt);
            }
        }

        for (List<Attempt__c> cAs:[SELECT Id, Certification_Id__c, Student__c, Number_of_Elements_for_Cert__c
                                         FROM Attempt__c
                                         WHERE Student__c IN :candidates
                                            AND Status__c = 'Complete/Pass' AND Element__r.Certification__c in:certifications]) {
            for (Attempt__c cA: cAs) {
                System.debug('Retrieved certAttempt id' + cA.id + ' candidate id'
                             + cA.Certification_Id__c + ' certification id' + cA.Student__c
                            + ' number of elements for cert ' + cA.Number_of_Elements_for_Cert__c);
                if(passCounts.containsKey(cA.Student__c+cA.Certification_Id__c)) {
                     passCounts.put(cA.Student__c+cA.Certification_Id__c, passCounts.get(cA.Student__c+cA.Certification_Id__c) + 1);
                } else {
                    passCounts.put(cA.Student__c+cA.Certification_Id__c, 1);
                }
            }
        }

        System.debug('Size of passCounts = ' + passCounts.size());

 		/** TODO #4: Create a for loop with an iterator of type Certification_Attempt__c named certAttempt
		 *           that iterates over the certAttemptsPass variable. Within the for loop:
 		 *           -- Declare a variable whose type is String named currentKey and assign it the value that results
		 *              from the concatenation of the candidate id and the certification id for the current record.
 		 *           -- an if statement that, if the number of elements for the certification matches the count for the
 		 *              corresponding currentKey in passCounts
         	 *              -- creates a certification held record
		 *              -- sets the date achieved field of the newly constructed certification held sObject
         	 *          	   to the completion date of the certification attempt.
		 *              -- sets the certification field of the newly constructed certification held sObject
		 *                 to the id of the certification that will be held.
		 *              -- sets the certificated professional field of the newly constructed certification held
		 *                 sObject to the id of the certification candidate.
         	 *              -- add it to the certsHeldToAdd Map, using the currentKey as the key value.
		 */
        for(Attempt__c certAttempt : certAttemptsPass){
             // If all certification elements are complete and if a certification held does not already exist
             String currentKey = certAttempt.Student__c+certAttempt.Certification_Id__c;
             if(passCounts.get(currentKey) == certAttempt.Number_Of_Elements_For_Cert__c){
                // Create a new certification held record
                Held__c certHeld = new Held__c();
                certHeld.Date_Achieved__c = certAttempt.Completion_Date__c;
                certHeld.Certification__c = certAttempt.Certification_Id__c;
                certHeld.Contact__c = certAttempt.Student__c;
                certsHeldToAdd.put(currentKey,certHeld);
            }
        }

        /** TODO #5: Create a try-catch block that:
		 *          -- In the try section:
		 *             -- an if statement that, if the number of certsHeldToAdd size is greater
		 *                than zero:
		 *                -- Use the insert DML statement to insert the values for the certsHeldToAdd variable.
		 *                -- Call the debug method of the System class, passing in the size of the certsHeldToAdd variable
         *          -- In the catch section, which should accept as a parameter a variable whose type
		 *             is System.DMLException named ex:
		 *             Call the debug method of the System class, passing in the ex variable.
		 */
        try {
        	if(certsHeldToAdd.size() > 0 ) {
                insert certsHeldToAdd.values();
                System.debug(certsHeldToAdd.size() + ' certification held records being added');
            }
        } catch (System.DmlException ex) {
            System.debug(ex);
        }

    }


	public static void grantInstructorSharingAccess(List<Attempt__c> triggerNew, Map<Id, Attempt__c> oldMap, Boolean isInsert, Boolean isUpdate) {

		/**
		  *  When a new certification attempt object is assigned an
		  *  instructor, or if an instructor changes on an existing
		  *  object, give the new instructor access to the record by
		  *  creating a share object, and remove access for the
		  *  previous instructor (if it is an update of that field)
		  */

		System.debug('Starting the Grant Instructor Sharing Access logic');

		// List of share records to insert in bulk
		List<Attempt__Share> sharesToCreate = new List<Attempt__Share>();

		// List of share records to delete in bulk
		List<Attempt__Share> sharesToDelete = new List<Attempt__Share>();

		// Map of the CertAttemptID to the Instructor User ID
		Map<Id, Id> certAttemptToInstructorMap = new Map<Id, Id>();

		// Loop through all the records in the trigger
		for ( Attempt__c certAttempt : triggerNew ) {

			// Check to see if this is an insert or the Instructor has changed
			if (Trigger.isInsert || certAttempt.Certifying_Instructor__c != oldMap.get(certAttempt.Id).Certifying_Instructor__c) {
				// Create new Share record for the Instructor and add to list
				if (certAttempt.Certifying_Instructor__c != null) {
					Attempt__Share certAttemptShare = new Attempt__Share(
										parentId = certAttempt.Id,
										userOrGroupId = certAttempt.Certifying_Instructor__c,
										rowCause = Schema.Attempt__Share.RowCause.Certifying_Instructor__c,
										accessLevel = 'Edit');
					sharesToCreate.add(certAttemptShare);
				}
			}

			if (Trigger.isUpdate) {
				// See the Instructor has changed
				if ( certAttempt.Certifying_Instructor__c != oldMap.get(certAttempt.Id).Certifying_Instructor__c ) {
					// Add to map of Instructor changes
					System.debug('certAttempt.Certifying_Instructor__c is: ' + certAttempt.Certifying_Instructor__c);
					certAttemptToInstructorMap.put(certAttempt.Id, certAttempt.Certifying_Instructor__c);
				 }
			}
		 }

		 if ( certAttemptToInstructorMap.size() > 0 ) {

			System.debug('certAttemptToInstructorMap is: ' + certAttemptToInstructorMap);

			for (Attempt__Share certAttemptShare :
											[SELECT UserOrGroupId, RowCause, ParentId, Id, AccessLevel
											   FROM Attempt__Share
											   WHERE ParentId IN :certAttemptToInstructorMap.keySet()
												 AND RowCause = 'Certifying_Instructor__c']){
				if (certAttemptToInstructorMap.get(certAttemptShare.ParentId) != certAttemptShare.UserOrGroupId){
					sharesToDelete.add(certAttemptShare);
				}
			}

		 }

		 try {
			if ( sharesToCreate.size() > 0 ) {
				insert sharesToCreate;
			}
			if ( sharesToDelete.size() > 0) {
					delete sharesToDelete;
			}
		 } catch (System.DmlException ex) {
				System.debug(ex);
		 }
	}

	public static void validateCertificationAttempt(List<Attempt__c> triggerNew) {

		/**
        *  If a Contact is trying to create a Certification Attempt
        *  for an element that was already marked as "Complete/Pass"
        *  or "In Progress", then do not allow the attempt to be
        *  created and pass back an error
        */

        System.debug('Starting the Validate Certification Attempt logic');

        // All Certification_Attempt__c records which are 'In Progress' OR 'Complete/Pass' will be stored here for comparison purposes
        Set<String> concatStudentElement = new Set<String>();
				Set<ID> candidateIDs = new Set<ID>();
        for (Attempt__c dbca : triggerNew) {
            candidateIDs.add(dbca.Student__c);
        }
        for(Attempt__c dbca : [Select Student__c,Element__c, Status__c
                                             From Attempt__c Where Status__c IN ('In Progress', 'Complete/Pass')
                                             AND Student__c IN :candidateIDs]){
            // We need to know 1. Which student 2. Which element they have attempted (lab or multiple choice) 3. The status
            concatStudentElement.add((String)dbca.Student__c + (String)dbca.Element__c + (String)dbca.Status__c);
        }

        for(Attempt__c ca : triggerNew){
            // If any element is In Progress or Complete/Pass, new attempts should not be allowed for those elements
            if(concatStudentElement.contains((String)ca.Student__c + (String)ca.Element__c + 'In Progress') ||
               concatStudentElement.contains((String)ca.Student__c + (String)ca.Element__c + 'Complete/Pass')){
                   ca.addError('Cannot attempt cert for element already in progress or completed');
               }
        }
    }
}