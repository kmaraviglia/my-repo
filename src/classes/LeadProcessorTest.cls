@isTest
private class LeadProcessorTest {

    @testSetup static void setup(){
        List<Lead> leads = new List<Lead>();
        for(Integer i = 0; i< 200; i++){
            leads.add(new Lead(FirstName = 'Prova', LastName ='Lead '+i, LeadSource = 'Test', Company='Company di prova'));
        }
        insert leads;
    }
    
    static testmethod void test() {        
        Test.startTest();
        LeadProcessor lp= new LeadProcessor();
        Id batchId = Database.executeBatch(lp);
        Test.stopTest();

        // after the testing stops, assert records were updated properly
        System.assertEquals(200, [select count() from lead where LeadSource = 'Dreamforce']);
    }
}