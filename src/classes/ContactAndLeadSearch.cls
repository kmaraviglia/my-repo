public class ContactAndLeadSearch {
    public static List<List< SObject>> searchContactsAndLeads(String a){
        List<List< SObject>> lista = [FIND :a IN ALL FIELDS 
                                      RETURNING Lead(FirstName, LastName), Contact(FirstName,LastName)];

        return lista;
    }
}