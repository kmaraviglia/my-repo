@isTest
public class AccountProcessorTest {
    @isTest static void testAccount(){

       Account a = new Account();
        a.Name = 'Test Account' ;
        insert a ;
        
        Contact cont = new Contact();
        cont.FirstName ='Bob';
        cont.LastName ='Masters';
        cont.AccountId = a.Id;
        Insert cont;

        List<Id> Ids = new List<Id>();
        Ids.add(a.Id);
       
        Test.startTest();
        AccountProcessor.countContacts(Ids); //test method of AccountProcessor
        Test.stopTest();
       
        System.assertEquals(a.Number_of_Contacts__c, 1);
      
        
        
            return;
    }

}