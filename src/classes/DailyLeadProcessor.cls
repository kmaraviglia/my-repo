global class DailyLeadProcessor implements Schedulable {

    global void execute(SchedulableContext ctx){
        List<Lead> leads = new List<Lead>([SELECT Name, Id FROM Lead WHERE LeadSource = NULL LIMIT 200]);
        for(Lead l : leads){
            l.LeadSource = 'Dreamforce';
        }
        update leads;
        System.debug('Numero di leads caricati' + leads.size());
    }
}