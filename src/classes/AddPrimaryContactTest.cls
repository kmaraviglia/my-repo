@isTest
public class AddPrimaryContactTest {

     @testSetup static void setup(){
        List<Account> accountsNY = new List<Account>();
        List<Account> accountsCA = new List<Account>();
        for(Integer i = 0; i< 50; i++){
            accountsNY.add(new Account(Name = 'Prova'+i, BillingState= 'NY'));
            accountsCA.add(new Account(Name = 'Prova2'+i, BillingState= 'CA'));
        }
        insert accountsNY;
        insert accountsCA;
    }
    
    static testmethod void testQueueable() {
		Contact cTest = new Contact(FirstName='Test');
        String State = 'CA';
    	AddPrimaryContact apc = new AddPrimaryContact(cTest, State);
        Test.startTest(); 
        System.enqueueJob(apc);
        Test.stopTest();  
        List<Account> a = new List<Account>();
        a = [select Id from account where BillingState = 'CA'];
        for(Account acc : a){
            System.assertEquals(cTest.AccountId, acc.Id);
        }
        
    }
}