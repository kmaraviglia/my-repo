global class AccountProcessor {

    @future public static void countContacts(List<Id> AccountIds){
        List<Account> accounts = [SELECT Name, Id FROM Account WHERE Id =:AccountIds];
        for(Account acc : accounts){
            Integer n = [SELECT Count() FROM Contact WHERE AccountId =:acc.Id];
            acc.Number_of_Contacts__c = n;
            update acc;
        }
        return;
    }
}