@isTest
public class ParkLocatorTest {
    @isTest static void test(){
        Test.setMock(WebServiceMock.class, new ParkServiceMock());
        List<String> result = ParkLocator.country('Germany');
        List<String> expected = new List<String>{'Park1','Park2','Park3'};
       
        System.assertEquals(expected, result);
        return;
    }
}