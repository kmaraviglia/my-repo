public class Sample {   
    
    public List<RadarData> data {get;set;}
    public sample() {
        data = new List<RadarData>();
        List<AggregateResult> conList = new List<AggregateResult>();
        conList = [SELECT Level__c, Count(id)numb  FROM Contact 
                   WHERE Level__c != Null
                   GROUP BY Level__c];
        
        for(AggregateResult con : conList) {
            data.add(new RadarData(String.valueOf(con.get('Level__c')), Integer.valueOf(con.get('numb')))); // Problem with variable DISC__c
        }
    }
    public class RadarData {
        String discName {get;set;}
        Decimal numb {get;set;}
        
        
        public RadarData(String discName, Decimal numb) {
            this.discName = discName;
            this.numb = numb;
            
        }
    }
}