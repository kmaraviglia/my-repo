@isTest
public class DailyLeadProcessorTest {
    
    public static String CRON_EXP = '0 0 1 * * ?';

    static testmethod void testScheduledJob() { 
        Test.startTest();
        List<Lead> leads = new List<Lead>();
        for(Integer i =0; i<200; i++){
            leads.add(new Lead(FirstName = 'Prova', LastName ='Lead '+i, LeadSource = NULL, Company='Company di prova'));
        }
        insert leads;
        
       
        String jobId = System.schedule('DailyLeadProcessor',CRON_EXP,new DailyLeadProcessor());
        
        Integer num= [SELECT Count() FROM Lead WHERE LeadSource = 'Dreamforce'];
        //System.assertEquals(200, num);
        Test.stopTest();
    }
}