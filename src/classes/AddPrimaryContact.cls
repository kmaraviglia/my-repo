public class AddPrimaryContact implements Queueable{
    private Contact c;
    private String state;
    
    public AddPrimaryContact(Contact c, String state){
        this.c=c;
        this.state=state;
    }
    
    public void execute(QueueableContext context){
        List<Account> accounts = new List<Account>();
		accounts = [SELECT Name FROM Account WHERE BillingState =:state LIMIT 200];
        for(Account a: accounts){
            c.AccountId = a.Id;
            Contact cloned= c.clone(true, false);
        }
    }

}