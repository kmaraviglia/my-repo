@isTest
private class AccountManagerTest {

    @isTest static void testGetAccount() {
        Id recordId = createTestRecord();
        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/Accounts/'+recordId+'/contacts';
        request.httpMethod = 'GET';
        RestContext.request = request;
        Account thisAccount = AccountManager.getAccount();
        System.assert(thisAccount != null);
        //System.assertEquals('Test record', thisAccount.Name);
    }
    
    static Id createTestRecord() {
        // Create test record
        Account accountTest = new Account(
            Name='Test record'
            );
        insert accountTest;
        return accountTest.Id;
    }

}