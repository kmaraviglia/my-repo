public with sharing class mylwcController {

    @AuraEnabled(cacheable=true)
    public static List<Contact> getContacts(Id accId){
        System.debug('get contacts');
        return [SELECT Id, FirstName, LastName, AccountId, Email, Phone FROM Contact WHERE AccountId=:accId]; 
    }

    @AuraEnabled(cacheable=true)
    public static List<Contact> searchContacts(String searchKey, Id accId){
        System.debug('search contacts');
        String key = '%' + searchKey + '%';
        return [SELECT Id, Name, Title, Phone, Email  FROM Contact WHERE Name LIKE :searchKey];
    }
}