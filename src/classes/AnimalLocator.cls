public class AnimalLocator {
	
    public static String getAnimalNameById(Integer id){
        Http http = new Http();
        String str;
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://th-apex-http-callout.herokuapp.com/animals/'+id );
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        if (response.getStatusCode() == 200) {
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            Map<String, Object> animals = (Map<String, Object>) results.get('animal');
            System.debug('Ricevuti i seguenti animali ' + animals);
            str = string.valueOf(animals.get('name'));
            System.debug('StrResp >>>>' + str);
        }
        return str;
    }
}