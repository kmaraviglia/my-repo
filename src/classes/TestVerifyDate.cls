@isTest
public class TestVerifyDate {
    @isTest static void checkDatesTest1(){
        Date d1 = Date.newInstance(2017, 12, 7);
        Date d2 = Date.newInstance(2017, 12, 12); 
        Date d = VerifyDate.CheckDates(d1, d2);
        System.assertEquals(d2, d);
    }
    
    @isTest static void checkDatesTest2(){
        Date d1 = Date.newInstance(2017, 12, 12);
        Date d2 = Date.newInstance(2017, 12, 7); 
        Date d = VerifyDate.CheckDates(d1, d2);
		Integer totalDays = Date.daysInMonth(d1.year(), d1.month());
		Date lastDay = Date.newInstance(d1.year(), d1.month(), totalDays);
		System.assertEquals(lastDay, d);
    }

}